const HDWalletProvider = require('truffle-hdwallet-provider')
const Web3 = require('web3')
const { interface, bytecode} = require('./compile')

const provider = new HDWalletProvider(
    'real capable path insect permit secret hill pupil mistake prize flip useless',
    'https://ropsten.infura.io/v3/cabe83a8b1a944149332c8c943beb862'
)

// Deployed At Ropsten
// Accounts:  [ '0x68918E820d4018Dd201Bd8E2bBCbB3e10d442f7E' ]
// Contract Address:  0x32532E37593137036B3F4b2e9ffEE14B68F03d62


const web3 = new Web3(provider);

const deploy = async () => {
    const accounts = await web3.eth.getAccounts();
    console.log('Accounts: ', accounts);
    
    const contract = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode, arguments: []})
    .send({gas: '1000000', from: accounts[0]})

    console.log(interface)
    console.log('Contract Address: ', contract.options.address)
};
deploy();